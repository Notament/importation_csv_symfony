# Csv Importation

Import csv into your Db

## Dépendances

- PHP >= 7.2
- Composer à jour
- NPM / Node

## Installation

```
$> cd csv/
$>composer install
```

## Lancement

```
$> docker-compose up
```

## MySql et Adminer

Adminer est accessible sur le port 8003.

Identifiants du serveur MySQL :
- Serveur : import-csv-mysql
- Utilisateur / Mot de passe : root/root
- Base de Donnée : csv

##DB Modif
Pour que l'application fonctionne :
    - On s'assure de configurer le .env, en mettant exactement cette ligne "DATABASE_URL=mysql://root:root@mysql/csv" à la place de la phrase par défaut
Si modifs Db faire les 3 commandes pour exporter la nouvelle Db :
	IL FAUT QUE DOCKER-COMPOSE SOIT ALLUMÉ
	$> docker-compose exec php-fpm bash
	$> cd ClickAndLootApi
	$> php bin/console make:migration
	$> php bin/console doctrine:migrations:migrate (Valider "yes")
	$> exit
    - Votre Db est maintenant à jour;
