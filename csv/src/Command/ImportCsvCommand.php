<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Town;

class ImportCsvCommand extends Command
{   
    protected $em;

    public function __construct(EntityManagerInterface $entityManager){
        $this->em = $entityManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this

        ->setName('app:import-data')

        ->setDescription('Import data into db from a csv')
        
        ->setHelp('This command allows you to import data from csv');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $row = 1;
        if (($handle = fopen("test.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                

                $town = new Town();
                
                $town->setName($data[5])
                ->setSlug($data[2])
                ->setPostalCode($data[8])
                ->setTownCode($data[10])
                ->setLongitude($data[19])
                ->setLatitude($data[20]);

                $this->em->persist($town);
                $this->em->flush();

                $row++;
            }
            fclose($handle);
        }

        //data[5] = name
        //data[2] = slug
        //data[8] = code_Postal
        //data[10] = town_code
        // data[19] = longitude
        // data[20] = latitude
    }
}